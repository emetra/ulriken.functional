﻿using System.Diagnostics;

namespace UlrikenResult;

[DebuggerStepThrough]
public static class Check
{
    public static CheckParamObj That(object value, string name = "")
    {
        return new CheckParamObj(value, name);
    }
    public static CheckParamString That(string value, string name = "")
    {
        return new CheckParamString(value, name);
    }

    public static CheckParamBool That(bool value, string name = "")
    {
        return new CheckParamBool(value, name);
    }
    public static CheckParamInt That(int value, string name = "")
    {
        return new CheckParamInt(value, name);
    }

}