﻿using System.Diagnostics;
using System.Text.RegularExpressions;
using UlrikenResult.Helpers;

namespace UlrikenResult;

[DebuggerStepThrough]
public class CheckParamString : Param<string>
{
    public CheckParamString(string value, string name = "") : base(value, name) { }

    public Result Matches(string regex, string message = "")
        => Regex.IsMatch(Value, regex)
            ? Result.Ok()
            : Result.Fail(message.IsEmpty() ? $"{Value} dont match {regex}" : message);

    public Result IsEmpty(string message = "") => Value.IsEmpty()
        ? Result.Ok()
        : Result.Fail(message.IsEmpty() ? $"{Name} should be empty" : message);

    public Result IsNotEmpty(string message = "") => Value.IsNotEmpty()
        ? Result.Ok()
        : Result.Fail(message.IsEmpty() ? $"{Name} should not be empty" : message);
}