﻿using System.Diagnostics;

namespace UlrikenResult;

/// <summary>
/// Base-class for param-extensions.
/// </summary>
/// <typeparam name="T"></typeparam>

[DebuggerStepThrough]
public class Param<T>
{
    protected readonly T Value;
    protected readonly string Name;

    protected Param(T value, string name = "")
    {
        Value = value;
        Name = name;
    }


}