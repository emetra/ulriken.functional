﻿using System.Diagnostics;
using UlrikenResult.Helpers;

namespace UlrikenResult;

[DebuggerStepThrough]
public class CheckParamBool : Param<bool>
{
    public CheckParamBool(bool value, string name = "") : base(value, name) { }

    public Result IsTrue(string message = "") =>
        !Value ? Result.Fail(message.IsEmpty() ? $"{Name}: expected true, but was false" : message) : Result.Ok();

    public Result IsFalse(string message = "") =>
        Value ? Result.Fail(message.IsEmpty() ? $"{Name}: expected false, but was true" : message) : Result.Ok();

    public CheckParamBool AndThat(bool value) => new CheckParamBool(Value && value, Name);
    public CheckParamBool OrThat(bool value) => new CheckParamBool(Value || value, Name);
}