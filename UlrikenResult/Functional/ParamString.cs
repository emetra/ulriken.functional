﻿using System.Diagnostics;
using System.Linq;
using UlrikenResult.Helpers;

namespace UlrikenResult;

[DebuggerStepThrough]
public class ParamString : Param<string>
{
    public ParamString(string value, string name = "") : base(value, name) { }

    public ParamString IsEmpty(string message = "")
    {
        Do.EnsureRaiseIf(() => !string.IsNullOrWhiteSpace(Value), message.IsEmpty() ? $"Please provide a value for {Name}" : message);
        return this;
    }

    public ParamString IsNotEmpty(string message = "")
    {
        Do.EnsureRaiseIf(() => string.IsNullOrWhiteSpace(Value),message.IsEmpty() ? $"{Name} should be empty" : message);
        return this;
    }

    public ParamString In(params string[] values)
    {
        Do.EnsureRaiseIf(() => !values.Contains(Value), $"The given list does not contain {Value}");
        return this;
    }
}