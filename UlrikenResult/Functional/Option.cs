﻿using System;
using System.Diagnostics;
using UlrikenResult.Helpers;

namespace UlrikenResult;

/// <summary>
/// Simple class for Option: brings "nullable" to all types
/// </summary>
/// <typeparam name="T"></typeparam>

[DebuggerStepThrough]
public class Option<T>
{
    private T Value { get; }
    public Option<TS> Match<TS>(
        Func<T, Option<TS>> okFunc,
        Func<Option<TS>> failFunc) =>
        HasValue ? okFunc(Value) : failFunc();

    public Option<T> Match(
        Func<T, Option<T>> okFunc,
        Func<Option<T>> failFunc) =>
        HasValue ? okFunc(Value) : failFunc();

    public T ValueOrDefault() => Value != null ? Value : default;
    public T ValueOrDefault(T val) => Value != null ? Value : val;

    public bool HasValue { get; }

    internal Option(T value)
    {
        HasValue = true;
        Value = value;
    }

    internal Option()
    {
        HasValue = false;
        Value = default;
    }

    public T Or(in T value)
    {
        Ensure.That(value).IsNotNull();
        return HasValue ? Value : value;
    }
}
 
[DebuggerStepThrough]
public static class Option
{
    public static Option<T> Some<T>(T value) => value.Some();
    public static Option<T> None<T>() => new Option<T>();
}