﻿using System;
using System.Diagnostics;

namespace UlrikenResult;

[Serializable]
[DebuggerStepThrough]
public class EnsureException : Exception
{
    public EnsureException(string msg) : base(msg) { }
}