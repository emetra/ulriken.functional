﻿using System.Diagnostics;

namespace UlrikenResult;

[DebuggerStepThrough]
public static class Ensure
{
    public static ParamInt That(int value, string name = "") => new ParamInt(value, name);
    public static ParamObj That(object value, string name = "") => new ParamObj(value, name);
    public static ParamString That(string value, string name = "") => new ParamString(value, name);
    public static ParamStrings That(params string[] values) => new ParamStrings(values);
    public static ParamBool That(bool value, string name = "") => new ParamBool(value, name);
    public static ParamObjects That(params object[] values) => new ParamObjects(values);
}

