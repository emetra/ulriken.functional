﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace UlrikenResult;

[DebuggerStepThrough]
public static class Do
{
    #region Private methods
    /// <summary>
    /// Throw ApplicationException with the given message.
    /// </summary>
    /// <param name="errorMessage"></param>
    /// <exception cref="ApplicationException"></exception>
    private static void Raise(string errorMessage)
    {
        throw new ApplicationException(errorMessage);
    }

    /// <summary>
    /// Throw EnsureException with the given exception
    /// </summary>
    /// <param name="errorMessage"></param>
    /// <exception cref="EnsureException"></exception>
    private static void EnsureRaise(string errorMessage)
    {
        throw new EnsureException(errorMessage);
    }

    /// <summary>
    /// If the given function returns true then throw new EnsureException with the given error-message.
    /// </summary>
    /// <param name="func"></param>
    /// <param name="errorMessage"></param>
    internal static void EnsureRaiseIf(Func<bool> func, string errorMessage)
    {
        if (func()) EnsureRaise(errorMessage);
    }
    #endregion

    #region Public methods

    /// <summary>
    /// If the given Result is failed, then:
    /// - If the result has an exception then throw this
    /// - If the result doesn't has an exception then throw a new ApplicationException with Result.Message
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="result"></param>
    /// <exception cref="ApplicationException"></exception>
    public static void RaiseIfFailed<T>(Result<T> result)
    {
        if (!result.Failed) return;
        if (result.Exception.HasValue) throw result.Exception.ValueOrDefault();
        throw new ApplicationException(result.Message);
    }

    public static void RaiseIfFailed(Result result)
    {
        if (!result.Failed) return;
        if (result.Exception.HasValue) throw result.Exception.ValueOrDefault();
        throw new ApplicationException(result.Message);
    }

    /// <summary>
    /// if given function returns true then throw new ApplicationException with the given message
    /// </summary>
    /// <param name="func"></param>
    /// <param name="errorMessage"></param>
    public static void RaiseIf(Func<bool> func, string errorMessage)
    {
        if (func()) Raise(errorMessage);
    }

    /// <summary>
    /// If given function returns true then throw the given exception
    /// </summary>
    /// <param name="func"></param>
    /// <param name="exception"></param>
    public static void RaiseIf(Func<bool> func, Exception exception)
    {
        if (func()) throw exception;
    }

    /// <summary>
    /// Execute the given function, and wrap it in a Result.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="func"></param>
    /// <param name="messageOnFail"></param>
    /// <returns></returns>
    public static Result<T> Try<T>(Func<T> func, string messageOnFail = "")
    {
        try
        {
            var result = func();
            return Result<T>.Ok(result);
        }
        catch (Exception exception)
        {
            var result = Result<T>.Fail(exception, messageOnFail);
            return result;
        }
    }

    /// <summary>
    /// Execute the given function, and wrap it in a Result.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="func"></param>
    /// <param name="messageOnFail"></param>
    /// <returns></returns>
    public static Result Try(Action func, string messageOnFail = "", string logMessage = "")
    {
        try
        {
            func();
            return Result.Ok();
        }
        catch (Exception exception)
        {
            var result = Result.Fail(exception, messageOnFail);
            return result;
        }
    }

    /// <summary>
    /// Execute function. Before func, execute before, always end with always.
    /// before and always never throws exceptions.
    /// </summary>
    /// <param name="before"></param>
    /// <param name="func"></param>
    /// <param name="always"></param>
    /// <param name="messageOnFail"></param>
    /// <returns></returns>
    public static Result Try(Action before, Action func, Action always, string messageOnFail = "")
    {
        try
        {
            try { before(); }
            catch
            {
                // ignored
            }

            return Try(func, messageOnFail);
        }
        finally
        {
            try
            {
                always();
            }
            catch
            {
                // ignored
            }

        }

    }

    public static Result<T> Try<T>(Action before, Func<T> func, Action always, string messageOnFail = "")
    {
        try
        {
            try { before(); }
            catch
            {
                // ignored
            }

            return Try(func, messageOnFail);
        }
        finally
        {
            try
            {
                always();
            }
            catch
            {
                // ignored
            }

        }

    }
    public static Task<Result> TryAsync(Action func, string messageOnFail = "", string logMessage = "")
    {
        return new TaskFactory().StartNew(() =>
        {
            try
            {
                func();
                return Result.Ok();
            }
            catch (Exception exception)
            {
                var result = Result.Fail(exception, messageOnFail);
                return result;
            }
        });
    }

    public static Task<Result<T>> TryAsync<T>(Func<T> func, string messageOnFail = "")
    {
        return new TaskFactory().StartNew(() =>
        {
            try
            {
                var result = func();
                return Result<T>.Ok(result);
            }
            catch (Exception exception)
            {
                var result = Result<T>.Fail(exception, messageOnFail);
                return result;
            }
        });
    }
    #endregion

}