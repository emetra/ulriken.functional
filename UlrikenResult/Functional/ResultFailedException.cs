﻿using System;

namespace UlrikenResult;

public class ResultFailedException : Exception
{
    public ResultFailedException(string message) : base(message)
    {

    }
    public ResultFailedException(string message, Exception ex) : base(message, ex)
    {

    }
}