﻿using System.Diagnostics;
using UlrikenResult.Helpers;

namespace UlrikenResult;

[DebuggerStepThrough]
public class ParamObj : Param<object>
{
    public ParamObj(object value, string name = "") : base(value, name) { }

    public ParamObj IsNotNull(string message = "")
    {
        Do.EnsureRaiseIf(() => Value == null, message.IsEmpty() ? $"Please provide a value for {Name}" : message);
        return this;
    }

    public ParamObj IsNull(string message = "")
    {
        Do.EnsureRaiseIf(() => Value != null, message.IsEmpty() ? $"Expected null for {Name}, but it has a value" : message);
        return this;
    }
}