﻿using UlrikenResult.Helpers;

namespace UlrikenResult;

public class ParamBool : Param<bool>
{
    public ParamBool(bool value, string name = "") : base(value, name) { }

    public ParamBool IsTrue(string message = "")
    {
        Do.EnsureRaiseIf(() => !Value, message.IsEmpty() ? $"{Name}: expected true, but was false" : message);
        return this;
    }

    public ParamBool IsFalse(string message = "")
    {
        Do.EnsureRaiseIf(() => Value, message.IsEmpty() ? $"{Name}: expected false, but was true" : message);
        return this;
    }
}