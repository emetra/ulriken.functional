﻿using System.Diagnostics;
using System.Linq;
using UlrikenResult.Helpers;

namespace UlrikenResult;

[DebuggerStepThrough]
public class CheckParamInt : Param<int>
{
    public CheckParamInt(int value, string name = "") : base(value, name) { }
    public Result<CheckParamInt> Is(int value, string message = "") =>
        Value == value
            ? this.Ok()
            : this.Fail(message.IsEmpty()
                ? $"Expected {value} for {Name}, but it has a value {Value}."
                : message);

    public Result IsNot(int value, string message = "")
        => Value != value
            ? Result.Ok()
            : Result.Fail(message.IsEmpty() ? $"Expected {value} for {Name}, but it has a value {Value}." : message);
    public Result IsIn(params int[] values)
        => values.ToList().Contains(Value)
            ? Result.Ok()
            : Result.Fail($"{Value} not in given list");
    public Result IsBetween(int from, int to)
        => Value.Between(from, to)
            ? Result.Ok()
            : Result.Fail($"{Value} not between {from} and {to}");

}