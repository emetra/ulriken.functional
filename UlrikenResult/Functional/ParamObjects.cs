﻿using System.Diagnostics;
using System.Linq;
using UlrikenResult.Helpers;

namespace UlrikenResult;

[DebuggerStepThrough]
public class ParamObjects : Param<object[]>
{
    public ParamObjects(params object[] value) : base(value, "")
    {
    }

    public ParamObjects NoneAreNull(string message = "")
    {
        if (Value == null) return this;
        Do.EnsureRaiseIf(() => Value == null, message.IsEmpty() ? $"Please provide a value for {Name}" : message);
        Value.ToList().ForEach(v => Do.EnsureRaiseIf(() => v == null, message.IsEmpty() ? $"Please provide a value for {Name}" : message));
        return this;
    }

    public ParamObjects AllAreNull(string message = "")
    {
        Do.EnsureRaiseIf(() => Value == null, message.IsEmpty() ? $"Please provide a value for {Name}" : message);
        Value.ToList().ForEach(v => Do.EnsureRaiseIf(() => v != null, message.IsEmpty() ? $"{Name} has a value, should be null" : message));
        return this;
    }
}