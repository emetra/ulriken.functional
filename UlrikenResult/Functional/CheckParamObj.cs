﻿using System.Diagnostics;
using UlrikenResult.Helpers;

namespace UlrikenResult;

[DebuggerStepThrough]
public class CheckParamObj : Param<object>
{
    public CheckParamObj(object value, string name = "") : base(value, name) { }
    public Result IsNotNull(string message = "") => Value == null ? Result.Fail(message.IsEmpty() ? $"Please provide a value for {Name}" : message) : Result.Ok();
    public Result IsNull(string message = "") => Value != null ? Result.Fail(message.IsEmpty() ? "Expected null for {Name}, but it has a value" : message) : Result.Ok();
}