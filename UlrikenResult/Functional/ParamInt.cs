﻿using System.Diagnostics;
using System.Linq;

namespace UlrikenResult;

[DebuggerStepThrough]
public class ParamInt : Param<int>
{
    public ParamInt(int value, string name = "") : base(value, name) { }

    public ParamInt Is(int value, string message = "")
    {
        Do.EnsureRaiseIf(() => Value != value, $"Expected {Value}, got {value}. {message}".Trim());
        return this;
    }

    public ParamInt IsNot(int value, string message = "")
    {
        Do.EnsureRaiseIf(() => Value == value, $"Expected different value than {Value}. {message}".Trim());
        return this;
    }

    public ParamInt IsLessThan(int value, string message = "")
    {
        Do.EnsureRaiseIf(() => Value >= value, $"Expected value less than {Value}. {message}".Trim());
        return this;
    }

    public ParamInt IsLessThanOrEqualTo(int value, string message = "")
    {
        Do.EnsureRaiseIf(() => Value > value, $"Expected value less than or equal to {Value}. {message}".Trim());
        return this;
    }

    public ParamInt IsGreaterThan(int value, string message = "")
    {
        Do.EnsureRaiseIf(() => Value <= value, $"Expected value greater than {Value}. {message}".Trim());
        return this;
    }

    public ParamInt IsGreaterThanOrEqualTo(int value, string message = "")
    {
        Do.EnsureRaiseIf(() => Value < value,$"Expected value greater than or equal to {Value}. {message}".Trim());
        return this;

    }

    public ParamInt IsIn(params int[] values)
    {
        Do.EnsureRaiseIf(() => !values.ToList().Contains(Value), "Value not in given list");
        return this;
    }
}