﻿using System.Linq;
using UlrikenResult.Helpers;

namespace UlrikenResult;

public class ParamStrings : Param<string[]>
{
    public ParamStrings(params string[] value) : base(value, "")
    {

    }

    public ParamStrings NoneAreEmpty(string message = "")
    {
        Do.EnsureRaiseIf(() => Value == null, message.IsEmpty() ? $"Please provide a value for {Name}" : message);
        Value.ToList().ForEach(v => Do.EnsureRaiseIf(v.IsEmpty, message.IsEmpty() ? $"Please provide a value for {Name}" : message));
        return this;
    }

}