using System;
using System.Diagnostics;
using UlrikenResult.Helpers;

namespace UlrikenResult;

/// <summary>
/// The Result-class. Use instead of 'void', and wrap the code in a Do.Try 
/// </summary>
[DebuggerStepThrough]
public class Result
{
    #region Private fields
    private string message;
    #endregion

    #region Public properties

    /// <summary>
    /// Success = true => No errors
    /// </summary>
    public bool Success { get; }

    /// <summary>
    /// Failed = true => The method fail. 'Message' could contain a message describing the error.
    /// </summary>
    public bool Failed => !Success;

    /// <summary>
    /// A message. If failed and an exception was thrown it also contains the exception.
    /// </summary>
    public string Message
    {
        get => $"{message}{(Exception.HasValue ? " " + Exception.ValueOrDefault() : "")}";
        private set => message = value;
    }

    /// <summary>
    /// If an exception was thrown it can be accessed here.
    /// </summary>
    public Option<Exception> Exception { get; }

    #endregion

    #region Public methods

    /// <summary>
    /// Execute the given action and return the result if Success = true, otherwise return self
    /// </summary>
    /// <param name="action">A method taking a result as a parameter, returning a result</param>
    /// <returns></returns>
    public Result Then(Func<Result, Result> action) => Success ? action(this) : this;


    public Result Then(Func<Result> action)
    {
        return Success ? action() : this;
    }

    public Result<TS> Then<TS>(Func<Result<TS>> action, string msg = "")
    {
        return !Success ? Result<TS>.Fail(Message) : action();
    }

    public Result Fail(Func<Result, Result> action)
    {
        if (Failed) action(this);
        return this;
    }

    public Result Fail(Action<Result> action)
    {
        if (Failed) action(this);
        return this;
    }

    public Result Always(Action action)
    {
        Do.Try(action);
        return this;
    }

    public Result<TS> Fail<TS>(Func<Result<TS>, Result<TS>> action) => (Result<TS>)(!Success ? action((Result<TS>)this) : this);

    public Result Fold(Func<bool> condition, Func<Result> falseFunction, Func<Result> trueFunction)
    {
        return Failed
            ? this
            : condition()
                ? trueFunction()
                : falseFunction();
    }

    #endregion

    #region Constructors

    private Result() { }

    protected Result(Exception exception, string message = "", bool success = false)
    {
        Ensure.That(exception).IsNotNull();
        Exception = exception.Some();
        Success = success;
        Message = message;
    }

    protected Result(string message = "", bool success = true)
    {
        Exception = Option.None<Exception>();
        Success = success;
        Message = message;
    }

    #endregion

    #region Static factories

    public static Result Ok(string message = "") => new Result(message);

    public static Result Fail(string message = "") => new Result(message, false);

    public static Result Fail(Exception exception, string message = "") => new Result(exception, message, false);

    #endregion
}


[DebuggerStepThrough]
public class Result<T> : Result
{
    #region Private properties

    private Option<T> Value { get; }

    #endregion

    #region Constructors

    private Result(Exception exception, string message = "", bool success = false) : base(exception, message, success)
        => Value = Option.None<T>();

    private Result(string message, bool success) : base(message, success)
        => Value = Option.None<T>();

    private Result(T result, string message = "", bool success = true) : base(message, success)
        => Value = result == null
            ? Option.None<T>()
            : result.Some();

    #endregion

    #region Public methods

    public Result<T> ThenIf(Predicate<T> predicate, Func<T, Result<T>> action)
        => !Success ? this : predicate(ValueOrDefault()) ? action(ValueOrDefault()) : this;

    public Result<TS> Then<TS>(Func<T, Result<TS>> action, string message = "")
    {
        return !Success 
            ? Result<TS>.Fail(Exception.ValueOrDefault(), Message)
            : action(ValueOrDefault());
    }

    public Result<TS> ThenTry<TS>(Func<T, TS> action)
        => !Success ? Result<TS>.Fail(Message) : Do.Try(() => action(ValueOrDefault()));

    public Result Then(Func<T, Result> action)
        => Failed ? this : action(Value.ValueOrDefault());

    public Result Then(Predicate<T> pred, Func<T, Result> action)
        => Failed
            ? this
            : pred(Value.ValueOrDefault())
                ? action(Value.ValueOrDefault())
                : this;

    public Result Fold(Predicate<T> condition, Func<T,Result> falseFunction, Func<T, Result> trueFunction)
    {
        return Failed
            ? this
            : condition(ValueOrDefault())
                ? trueFunction(ValueOrDefault())
                : falseFunction(ValueOrDefault());
    }

    public Result<TS> Fold<TS>(Predicate<T> condition, Func<T, Result<TS>> falseFunction, Func<T, Result<TS>> trueFunction)
    {
        return Failed
            ? Result<TS>.Fail(Exception.ValueOrDefault(), Message)
            : condition(ValueOrDefault())
                ? trueFunction(ValueOrDefault())
                : falseFunction(ValueOrDefault());
    }

    public new Result<T> Always(Action action)
    {
        Do.Try(action);
        return this;
    }

    public Result<T> Always(Action<T> action)
    {
        if (!Failed) action(ValueOrDefault());
        return this;
    }

    public T ValueOrDefault() => Value.ValueOrDefault();

    #endregion

    #region Static Factories

    public new static Result<T> Fail(Exception exception, string message = "") => new Result<T>(exception, message);

    public new static Result<T> Fail(string message = "") => new Result<T>(message, false);

    public static Result<T> Ok(T value, string message = "") => new Result<T>(value, message);

    #endregion

    public T ValueOr(T value) => Value.HasValue ? Value.ValueOrDefault() : value;

    public T ValueOrRaise(string errorMessage)
    {
        if (!Success && Exception.HasValue) throw new ResultFailedException(errorMessage, Exception.ValueOrDefault());
        if (!Success) throw new ResultFailedException(errorMessage + " " + Message);
        if (!Value.HasValue) throw new ResultFailedException(errorMessage);
        return Value.ValueOrDefault();
    }

    public T ValueOrRaise(Exception exception)
    {
        if (!Success) throw exception;
        if (!Value.HasValue) throw exception;
        return Value.ValueOrDefault();
    }
}