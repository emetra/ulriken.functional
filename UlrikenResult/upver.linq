<Query Kind="Program" />

void Main(string[] args)
{
#if CMD
   	  var newVersion =  args.Length > 0 ? args[0] : DateTime.Now.ToString("yy.MM.dd.") + "1";
#else
	var newVersion = DateTime.Now.ToString("yy.MM.dd.") + "1";
#endif

	var dir = Directory.GetCurrentDirectory();
	var csproj = Directory.EnumerateFiles(dir, "*.csproj");
	if (csproj.Count() != 1)
	{
		"Må kun være en prosjekt-fil i mappen!".Dump();
		Environment.Exit(0);
	}


	var xml = XDocument.Load(csproj.FirstOrDefault());
	var currentVersion = xml.Root.Element("PropertyGroup").Element("AssemblyVersion").Value;

	var version = currentVersion.Split('.').Select(l => new { Major = l[0], Minor = l[1], Patch = l[2] }).FirstOrDefault();

	if (currentVersion.Substring(0, 9) == DateTime.Now.ToString("yy.MM.dd."))
		newVersion = (currentVersion.Substring(0, 9) + (Convert.ToInt32(currentVersion.Remove(0, 9)) + 1).ToString());

	$"Endrer versjon fra {currentVersion} til {newVersion}".Dump();

	xml.Root.Element("PropertyGroup").SetElementValue("AssemblyVersion", newVersion);
	xml.Root.Element("PropertyGroup").SetElementValue("FileVersion", newVersion);
	xml.Save(csproj.FirstOrDefault());
}

// Define other methods and classes here
