﻿namespace UlrikenResult.Helpers;

public static class CheckParamExtensions
{
    public static CheckParamInt And(this CheckParamInt @this) => @this;
}