﻿using System;
using System.Linq;

namespace UlrikenResult.Helpers;

public static class ContentExtensions
{
    public static bool Between<T>(this T @this, T low, T high) where T : IComparable<T>
        => low.CompareTo(high) < 0 && @this.CompareTo(low) >= 0 && @this.CompareTo(high) <= 0;

    public static bool In<T>(this T @this, params T[] items)
        => items.Contains(@this);
}