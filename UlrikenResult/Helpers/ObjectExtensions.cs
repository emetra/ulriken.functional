﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace UlrikenResult.Helpers;

[DebuggerStepThrough]
public static class ObjectExtensions
{
    #region Public
    public static T ShallowCopy<T>(this T @this) => (T)@this.GetType().GetTypeInfo().GetMethod("MemberwiseClone", BindingFlags.Instance | BindingFlags.NonPublic)?.Invoke(@this, null);

    public static T With<T>(this T @this, string propertyName, object newValue) where T : class
    {
        var @new = @this.Copy();
        typeof(T).GetBackingField(propertyName)
            .SetValue(@new, newValue);
        return @new;
    }

    public static object Copy(this object @this)
        => InternalCopy(@this, new Dictionary<object, object>(new ReferenceEqualityComparer()));

    #endregion

    #region Private

    private static IEnumerable<FieldInfo> GetAllFields(this Type @this)
    {
        const BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static | BindingFlags.Instance | BindingFlags.DeclaredOnly;
        return @this == null
            ? Enumerable.Empty<FieldInfo>()
            : @this.GetFields(flags).Concat(@this.BaseType.GetAllFields());
    }

    private static FieldInfo GetBackingField(this Type @this, string propertyName) => @this.GetAllFields().FirstOrDefault(f => f.Name == $"<{propertyName}>k__BackingField");
    private static string BackingFieldName(string propertyName) => $"<{propertyName}>k__BackingField";

    private static string MemberName<T, TS>(this Expression<Func<T, TS>> e) => ((MemberExpression)e.Body).Member.Name;

    public static T With<T, TS>(this T @this, Expression<Func<T, TS>> exp, object newValue) where T : class
        => @this.With(exp.MemberName(), newValue);

    private static readonly MethodInfo CloneMethod
        = typeof(object).GetMethod("MemberwiseClone", BindingFlags.NonPublic | BindingFlags.Instance);

    private static bool IsPrimitive(this Type type)
        => (type == typeof(string) || (type.IsValueType & type.IsPrimitive));


    private static object InternalCopy(object obj, IDictionary<object, object> visited)
    {
        if (obj == null) return null;
        var typeToReflect = obj.GetType();
        if (IsPrimitive(typeToReflect)) return obj;
        if (visited.ContainsKey(obj)) return visited[obj];
        if (typeof(Delegate).IsAssignableFrom(typeToReflect)) return null;
        var clone = CloneMethod.Invoke(obj, null);
        if (typeToReflect.IsArray)
        {
            var arrayType = typeToReflect.GetElementType();
            if (IsPrimitive(arrayType) == false)
            {
                var clonedArray = (Array)clone;
                clonedArray.ForEach((array, indices) => array.SetValue(InternalCopy(clonedArray.GetValue(indices), visited), indices));
            }

        }
        visited.Add(obj, clone);
        CopyFields(obj, visited, clone, typeToReflect);
        RecursiveCopyBaseTypePrivateFields(obj, visited, clone, typeToReflect);
        return clone;
    }

    private static void RecursiveCopyBaseTypePrivateFields(object obj, IDictionary<object, object> visited, object cloneobject, Type typeToReflect)
    {
        if (typeToReflect.BaseType == null) return;
        RecursiveCopyBaseTypePrivateFields(obj, visited, cloneobject, typeToReflect.BaseType);
        CopyFields(obj, visited, cloneobject, typeToReflect.BaseType, BindingFlags.Instance | BindingFlags.NonPublic, info => info.IsPrivate);
    }

    private static void CopyFields(object obj, IDictionary<object, object> visited, object clone, Type typeToReflect, BindingFlags bindingFlags = BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.FlattenHierarchy, Func<FieldInfo, bool> filter = null)
    {
        foreach (var fieldInfo in typeToReflect.GetFields(bindingFlags))
        {
            if (filter != null && filter(fieldInfo) == false) continue;
            if (IsPrimitive(fieldInfo.FieldType)) continue;
            var originalFieldValue = fieldInfo.GetValue(obj);
            var clonedFieldValue = InternalCopy(originalFieldValue, visited);
            fieldInfo.SetValue(clone, clonedFieldValue);
        }
    }
    public static T Copy<T>(this T @this)
    {
        return (T)Copy((object)@this);
    }
    #endregion
}

public class ReferenceEqualityComparer : EqualityComparer<object>
{
    public override bool Equals(object x, object y) => ReferenceEquals(x, y);
    public override int GetHashCode(object obj) => obj == null ? 0 : obj.GetHashCode();
}

public static class ArrayExtensions
{
    public static void ForEach(this Array array, Action<Array, int[]> action)
    {
        if (array.LongLength == 0) return;
        var walker = new ArrayTraverse(array);
        do action(array, walker.Position);
        while (walker.Step());
    }
}

internal class ArrayTraverse
{
    public readonly int[] Position;
    private readonly int[] maxLengths;

    public ArrayTraverse(Array array)
    {
        maxLengths = new int[array.Rank];
        for (var i = 0; i < array.Rank; ++i)
        {
            maxLengths[i] = array.GetLength(i) - 1;
        }
        Position = new int[array.Rank];
    }

    public bool Step()
    {
        for (var i = 0; i < Position.Length; ++i)
        {
            if (Position[i] >= maxLengths[i]) continue;
            Position[i]++;
            for (var j = 0; j < i; j++)
            {
                Position[j] = 0;
            }
            return true;
        }
        return false;
    }
}

