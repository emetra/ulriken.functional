﻿namespace UlrikenResult.Helpers;

public static class ParamExtensions
{
    public static ParamInt And(this ParamInt @this) => @this;
    public static ParamObj And(this ParamObj @this) => @this;
    public static ParamString And(this ParamString @this) => @this;

}