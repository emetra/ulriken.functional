﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace UlrikenResult.Helpers;

[DebuggerStepThrough]
public static class ResultExtensions
{
    /// <summary>
    /// Given a result, return a one with IsFailed if not already failed.
    /// </summary>
    /// <param name="this"></param>
    /// <param name="message"></param>
    /// <returns></returns>
    public static Result ToFailure(this Result @this, string message)
        => @this.Success ? Result.Fail(message) : @this;

    public static Result<T> ToFailure<T>(this Result<T> @this, string message)
        => @this.Success ? Result<T>.Fail(message) : @this;

    public static Result<T> Ok<T>(this T @this) => Result<T>.Ok(@this);
    public static Result<T> Fail<T>(this T @this, string message) => Result<T>.Fail(message);

    /// <summary>
    /// Given a list of result, return a Result containing a list instead. If one of the results
    /// is failed the returned result will also be failed. All results must have status Success
    /// or the returned result will have status IsFailed.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="this"></param>
    /// <returns></returns>
    public static Result<List<T>> Flatten<T>(this List<Result<T>> @this)
        => Do.Try(() =>
        {
            Ensure.That(@this).IsNotNull("Please provide a list of results that needs to be flattened");
            var enumerable = @this.ToList();
            Ensure.That(enumerable.All(r => r.Success)).IsTrue("Cannot flatten a list containing failed results!");
            return new List<T>(enumerable.Select(r => r.ValueOrDefault()));
        });

    public static Result<IEnumerable<T>> Flatten<T>(this IEnumerable<Result<T>> @this)
        => Do.Try(() =>
        {
            Ensure.That(@this).IsNotNull("Please provide a list of results that needs to be flattened");
            var enumerable = @this.ToList();
            Ensure.That(enumerable.All(r => r.Success)).IsTrue("Cannot flatten a list containing failed results!");
            return (IEnumerable<T>)new List<T>(enumerable.Select(r => r.ValueOrDefault()));
        });
    /// <summary>
    /// Given a list of results, and a mapper-function, return a new list of the mapped results.
    /// </summary>
    /// <typeparam name="T1"></typeparam>
    /// <typeparam name="T2"></typeparam>
    /// <param name="this"></param>
    /// <param name="func"></param>
    /// <returns></returns>
    public static List<Result<T2>> Map<T1, T2>(this List<Result<T1>> @this, Func<Result<T1>, Result<T2>> func)
        => @this.Select(func).ToList();
}