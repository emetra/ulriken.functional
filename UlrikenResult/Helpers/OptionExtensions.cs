﻿using System.Diagnostics;

namespace UlrikenResult.Helpers;

[DebuggerStepThrough]
public static class OptionExtensions
{
    public static Option<T> Some<T>(this T @this) => new Option<T>(@this);
}