﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;

namespace UlrikenResult.Helpers;

public static class Extensions
{
    public static DateTime Trim(this DateTime @this, long roundTicks)
    {
        return new DateTime(@this.Ticks - @this.Ticks % roundTicks, @this.Kind);
    }

    public static void RunNTimes(this Action @this, int n) => Enumerable.Range(1, n).ToList().ForEach(i => @this());

    private static long EpochOf(this DateTime dt)
        => (long)(dt.ToUniversalTime() - new DateTime(1970, 1, 1)).TotalSeconds;

    public static string Content(this string @this)
    {
        using (var fs = new FileStream(@this, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
        {
            using (var sr = new StreamReader(fs, Encoding.Default))
            {
                return sr.ReadToEnd();
            }
        }
    }

    public static List<string> NonEmptyLines(this string @this)
        => @this
            .Content()
            .Split('\n')
            .Where(l => l.IsNotEmpty())
            .ToList();


    public static byte[] Compress(this byte[] raw, CompressionLevel compressionLevel)
    {
        using (var memory = new MemoryStream())
        {
            using (var gzip = new GZipStream(memory, compressionLevel, true))
            {
                gzip.Write(raw, 0, raw.Length);
                return memory.ToArray();
            }
        }
    }

}