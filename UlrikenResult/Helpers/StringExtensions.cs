﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

// ReSharper disable PossibleNullReferenceException

namespace UlrikenResult.Helpers;

[DebuggerStepThrough]
public static class StringExtensions
{
    #region Private classes

    private sealed class ServerAndDatabase
    {
        public string Server { get; }
        public string Database { get; }

        public ServerAndDatabase(string server, string database)
        {
            Server = server;
            Database = database;
        }
    }
    #endregion

    #region Private functions

    #endregion

    #region Public functions
    /// <summary>
    /// Given a list of strings this function creates a temporary file, and then write the lines to the file.
    /// </summary>
    /// <param name="lines"></param>
    /// <returns>The path and name of the new file</returns>
    public static string CreateInRandomFile(this IEnumerable<string> lines)
    {
        var file = Path.GetTempFileName();
        file.WriteToFile(lines);
        return file;
    }

    /// <summary>
    /// Given a list of strings and a file write the lines to the file.
    /// </summary>
    /// <param name="this"></param>
    /// <param name="lines"></param>
    /// <returns></returns>
    public static string WriteToFile(this string @this, IEnumerable<string> lines)
    {
        File.WriteAllLines(@this, lines);
        return @this;
    }

    /// <summary>
    /// Delete the given file if it exists
    /// </summary>
    /// <param name="this"></param>
    /// <returns></returns>
    public static string DeleteIfExists(this string @this)
    {
        if (File.Exists(@this)) File.Delete(@this);
        return @this;
    }

    public static FileInfo GetFileInfo(this string @this)
        => new FileInfo(@this);

    public static List<string> GetFileContents(this string @this) => File.ReadLines(@this).ToList();

    /// <summary>
    /// Returns the given string if not empty, otherwise return the given value.
    /// </summary>
    /// <param name="this"></param>
    /// <param name="defaultValue"></param>
    /// <returns>@this, or defaultValue if @this is empty</returns>
    public static string OrDefault(this string @this, string defaultValue) => @this.IsEmpty() ? defaultValue : @this;

    /// <summary>
    /// Check if a string is empty, null or whitespace. If so, returns true, otherwise false.
    /// </summary>
    /// <param name="this"></param>
    /// <returns></returns>
    public static bool IsEmpty(this string @this) => string.IsNullOrWhiteSpace(@this);

    /// <summary>
    /// Return true if given string is not empty, null or whitespace only.
    /// </summary>
    /// <param name="this"></param>
    /// <returns></returns>
    public static bool IsNotEmpty(this string @this) => !string.IsNullOrWhiteSpace(@this);

    /// <summary>
    /// Given a filename this function returns the name only (with extension), without the path.
    /// </summary>
    /// <param name="this"></param>
    /// <returns></returns>
    public static string GetFileName(this string @this) => @this.Contains('\\') ? @this.Split('\\').Last() : @this;

    /// <summary>
    /// Check if a fødselsnummer is valid.
    /// </summary>
    /// <param name="fnr"></param>
    /// <returns>Result.Success if valid, Result.Failed if not valid</returns>
    public static Result IsValidNationalId(this string fnr)
    {
        if (fnr.Length != 11) return Result.Fail("Må være 11 siffer i et fødselsnummer");

        // Valid date?  D-Number = +4
        var date = (fnr[0] <= '3') ? fnr.Substring(0, 6) : ((fnr[0] - '4') + fnr.Substring(1, 5));
        if (DateTime.TryParseExact(date, "ddMMyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out _) == false)
            return Result.Fail("Ugyldig dato");

        var n = new int[11];
        for (var i = 0; i < 11; i++)
        {
            if (int.TryParse(fnr[i].ToString(), out var tmp2))
                n[i] = tmp2;
            else
                return Result.Fail("Ugyldig tegn");
        }

        // Control number 1
        var k1 = 11 - (3 * n[0] + 7 * n[1] + 6 * n[2] + 1 * n[3] + 8 * n[4] + 9 * n[5] + 4 * n[6] + 5 * n[7] + 2 * n[8]) % 11;
        if (k1 == 11) k1 = 0;

        if (k1 == 10 || k1 != n[9]) return Result.Fail("Ugyldig kontrollsiffer");

        // Control number 2
        var k2 = 11 - (5 * n[0] + 4 * n[1] + 3 * n[2] + 2 * n[3] + 7 * n[4] + 6 * n[5] + 5 * n[6] + 4 * n[7] + 3 * n[8] + 2 * k1) % 11;
        if (k2 == 11) k2 = 0;
        if (k2 == 10 || k2 != n[10]) return Result.Fail("Ugyldig kontrollsiffer");

        return Result.Ok();
    }
    #endregion
}