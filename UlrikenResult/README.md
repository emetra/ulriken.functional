# UlrikenResult

## What is it?

UlrikenResult is methods and extensions helping you writing c# in a more functional manner.
It is my own helpers and extensions which I have collected into one assembly.

## How can you use it?

### The Result-class

The `Result`-class is like the `Either`-monad in Haskell. Combined with the static `Do`-function
I can write code that handles exceptions. I use it like this:

```csharp

public Result MyMethod(int aParameter) => Do.Try(()=>
{
    // Do something wich throws an exception
});


public void CallMyMethod()
{
    MyMethod(1)
        .Then(()=> /* MyMethod didnt throw an exception */)
        .Fail(r => /* MyMethod throw an exception, can be accessed in r.Exception */)
        .Finally(() => /* This code is always executed, even if MyMethod fails */);
}
```

### The Do-class

The `Do`-class is used as a wrapper around your methods. It's static, and you can add a logger to it so that
all exceptions are logged.

It has the following methods:

#### RaiseIf

`void RaiseIf(Func<bool> func, string errorMessage)`

This raises an application-exception with the given message if func is false.

`void RaiseIf(Func<bool> func, Exception exception)`

This raises the given exception if func is false.

#### Try

`Result<T> Try<T>(Func<T> func, string messageOnFail = "", string logMessage = "")`

`Result Try(Action func, string messageOnFail = "", string logMessage = "")`



