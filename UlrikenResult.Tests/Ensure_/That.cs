﻿using Shouldly;
using System;
using System.Collections.Generic;
using Xunit;
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace UlrikenResult.Tests.Ensure_;

public class TestObject
{
    public int? AnInt { get; set; }
    public string? AString { get; set; }
}

public class That
{
    [Fact]
    public void ShouldHandleInt()
    {
        var shouldNotThrow = new List<Action>
        {
            () => Ensure.That(1).Is(1),
            () => Ensure.That(1).IsNot(0),
            () => Ensure.That(0).IsNot(1),
            () => Ensure.That(0).IsIn(0, 1, 2, 3),
            () => Ensure.That(1).IsIn(0, 1, 2, 3),
            () => Ensure.That(2).IsIn(0, 1, 2, 3),
            () => Ensure.That(3).IsIn(0, 1, 2, 3),
            () => Ensure.That(1).IsLessThan(2),
            () => Ensure.That(1).IsLessThanOrEqualTo(2),
            () => Ensure.That(2).IsLessThanOrEqualTo(2),
            () => Ensure.That(3).IsGreaterThan(2),
            () => Ensure.That(3).IsGreaterThanOrEqualTo(3)
        };

        var shouldThrow = new List<Action>
        {
            () => Ensure.That(1).Is(0),
            () => Ensure.That(0).Is(1),
            () => Ensure.That(1).IsNot(1),
            () => Ensure.That(-1).IsIn(0, 1, 2, 3),
            () => Ensure.That(4).IsIn(0, 1, 2, 3),
            () => Ensure.That(2).IsLessThan(1),
            () => Ensure.That(3).IsLessThanOrEqualTo(2),
            () => Ensure.That(3).IsGreaterThan(3),
            () => Ensure.That(2).IsGreaterThanOrEqualTo(3)
        };

        shouldNotThrow.ForEach(action => action.ShouldNotThrow());
        shouldThrow.ForEach(action => action.ShouldThrow<EnsureException>());
    }

    [Fact]
    public void ShouldHandleStrings()
    {
        var shouldNotThrow = new List<Action>
        {
            () => Ensure.That("").IsEmpty(),
            () => Ensure.That((string?) null).IsEmpty(),
            () => Ensure.That("s").IsNotEmpty(),
            () => Ensure.That("s").In("s", "p")
        };

        var shouldThrow = new List<Action>
        {
            () => Ensure.That("a").IsEmpty(),
            () => Ensure.That((string?) null).IsNotEmpty(),
            () => Ensure.That("").IsNotEmpty(),
            () => Ensure.That("t").In("s", "p")
        };

        shouldNotThrow.ForEach(action => action.ShouldNotThrow());
        shouldThrow.ForEach(action => action.ShouldThrow<EnsureException>());
    }

    [Fact]
    public void ShouldHandleObjects()
    {
        var obj1 = new TestObject {AnInt = 1, AString = "1"};
        TestObject? obj2 = null;
        
        var shouldNotThrow = new List<Action>
        {
            () => Ensure.That(obj1).IsNotNull(),
            () => Ensure.That(obj2).IsNull()
        };
        
        var shouldThrow = new List<Action>
        {
            () => Ensure.That(obj1).IsNull(),
            () => Ensure.That(obj2).IsNotNull()
        };
        
        shouldNotThrow.ForEach(action => action.ShouldNotThrow());
        shouldThrow.ForEach(action => action.ShouldThrow<EnsureException>());
    }

    [Fact]
    public void ShouldHandleStringArray()
    {
        var shouldNotThrow = new List<Action>
        {
            () => Ensure.That("1","2","3").NoneAreEmpty(),
            () => Ensure.That("1","2","3").NoneAreEmpty()
        };

        var shouldThrow = new List<Action>
        {
            () => Ensure.That("","2","3").NoneAreEmpty(),
            () => Ensure.That("1",null,"3").NoneAreEmpty()
        };
        shouldNotThrow.ForEach(action => action.ShouldNotThrow());
        shouldThrow.ForEach(action => action.ShouldThrow<EnsureException>());
    }

    [Fact]
    public void ShouldHandleBooleans()
    {
        var shouldNotThrow = new List<Action>
        {
            () => Ensure.That(true).IsTrue("a"),
            () => Ensure.That(false).IsFalse("b")
            //() => Ensure.That(false).IsFalse("c").AndThat(true).IsTrue("d"),
            //() => Ensure.That(true).IsTrue("e").AndThat(false).IsFalse("f"),
            //() => Ensure.That(true).IsTrue("g").AndThat(true).IsTrue("h"),
            //() => Ensure.That(false).IsFalse("i").AndThat(false).IsFalse("j")
        };

        var shouldThrow = new List<Action>
        {
            () => Ensure.That(false).IsTrue("1"),
            () => Ensure.That(true).IsFalse("2")
            //() => Ensure.That(true).IsFalse("3").AndThat(true).IsTrue("4"),
            //() => Ensure.That(false).IsTrue("5").AndThat(false).IsFalse("6"),
            //() => Ensure.That(false).IsTrue("7").AndThat(true).IsTrue("8"),
            //() => Ensure.That(true).IsFalse("9").AndThat(false).IsFalse("10")
        };
        shouldNotThrow.ForEach(action =>
        {
            action.ShouldNotThrow();
        });
        shouldThrow.ForEach(action =>
        {
            action.ShouldThrow<EnsureException>();
        });
    }
}