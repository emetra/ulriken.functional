﻿using System;
using Shouldly;
using Xunit;

namespace UlrikenResult.Tests.Do_;

public class RaiseIf
{
    [Fact]
    public void ShouldRaiseIfTrue()
    {
        ((Action)(() => Do.RaiseIf(() => true, ""))).ShouldThrow<ApplicationException>();
    }
    [Fact]
    public void ShouldRaiseIfFalse()
    {
        ((Action)(() => Do.RaiseIf(() => false, ""))).ShouldNotThrow();
    }
}