﻿using Shouldly;
using System;
using Xunit;

namespace UlrikenResult.Tests.Do_;

public class RaiseIfFailed
{

    [Fact]
    public void ShouldThrowApplicationExceptionWhenResultIsFailed()
    {
        var result = Result.Fail("Error-message");
        Action action = () => Do.RaiseIfFailed(result);
        var exception = action.ShouldThrow<ApplicationException>();
        exception.Message.ShouldBe("Error-message");
    }

    [Fact]
    public void ShouldThrowApplicationExceptionWhenResultIsFailedAndNoMessage()
    {
        var result = Result.Fail();
        Action action = () => Do.RaiseIfFailed(result);
        var exception = action.ShouldThrow<ApplicationException>();
        exception.Message.ShouldBeEmpty();
    }
}
