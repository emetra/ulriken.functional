﻿using Shouldly;
using Xunit;

namespace UlrikenResult.Tests.Do_;

public class Try
{
    [Fact]
    public void ShouldNotFailInBeforeAndAlways()
    {
        var result = Do.Try(() => Ensure.That(true).IsFalse(), () => true, () => Ensure.That(true).IsFalse());
        result.Success.ShouldBeTrue();
        result.ValueOrDefault().ShouldBeTrue();
    }
    
    [Fact]
    public void ShouldNotFailInFunc()
    {
        var result = Do.Try(() => Ensure.That(true).IsFalse(), () => Ensure.That(true).IsFalse(), () => Ensure.That(true).IsFalse());
        result.Success.ShouldBeFalse();
        result.Exception.HasValue.ShouldBeTrue();
        result.Exception.ValueOrDefault().ShouldBeOfType<EnsureException>();
    }

    [Fact]
    public void ShouldReturnSuccessIfNotFailed()
    {
        Do.Try(()=> Ensure.That(1).Is(1)).Success.ShouldBeTrue();
    }

    [Fact]
    public void ShouldReturnFailedIfFailed()
    {
        Do.Try(() => Ensure.That(1).Is(0)).Success.ShouldBeFalse();
        Do.Try(() => Ensure.That(1).Is(0)).Exception.HasValue.ShouldBeTrue();
        Do.Try(() => Ensure.That(1).Is(0)).Exception.ValueOrDefault().ShouldBeOfType<EnsureException>();
    }
}